import { configureStore } from "@reduxjs/toolkit/react";
import { useDispatch, TypedUseSelectorHook, useSelector } from "react-redux";
import localforage from "localforage";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
  persistReducer,
  persistStore,
} from "redux-persist";
import reducers from "./reducers";
import storage from "redux-persist/lib/storage";

let storageLocal = localforage.createInstance({
  driver: localforage.INDEXEDDB,
  name: "MoomsApps",
  version: 1.0,
  size: 4980736,
  storeName: "MooMDB",
  description: "some description",
});

const persistConfig = {
  key: "M00MZ",
  version: 1.0,
  storage: typeof window === "undefined" ? storage : storageLocal,
  whitelist: ["token"],
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
  devTools: process.env.NODE_ENV === "development",
});

export const psStore = persistStore(store);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
