import ApiService from "./service";
import {
  AsyncThunk,
  Reducer,
  createAsyncThunk,
  createSlice,
} from "@reduxjs/toolkit";
import { Method } from "axios";
import { Request, Response, ThunkConfig } from "./entity";

export interface ServiceProps {
  name: string;
  endpoint: string;
  method: Method;
  useAuth?: boolean;
  baseUrl?: string;
}

export interface TypeService<Req, Res> {
  service: AsyncThunk<Response<Res>, Request<Req>, ThunkConfig>;
  reducer: Reducer<Response<Res>>;
}

export interface ReducerType<Res> {
  [key: string]: Reducer<Response<Res>>;
}

export interface ServiceType<Req, Res> {
  [key: string]: AsyncThunk<Response<Res>, Request<Req>, ThunkConfig>;
}

export interface EndpointType {
  [key: string]: TypeService<Request, Response>;
}

export const Slicer = <Req, Res>({
  name,
  endpoint,
  method,
  useAuth,
  baseUrl,
}: ServiceProps): TypeService<Req, Res> => {
  const service = createAsyncThunk<Response<Res>, Request<Req>, ThunkConfig>(
    name,
    async (data, thunkAPI) => {
      try {
        // const formData = new FormData();
        // formData.append("", new Blob([], ));

        const response = await ApiService<Response<Res>>({
          endpoint: `${endpoint}${data.additionalPath ?? ""}`,
          method: method,
          data: data.data,
          params: data.params,
          useAuth: useAuth,
          baseUrl: baseUrl,
        });
        return thunkAPI.fulfillWithValue(response);
      } catch (e: any) {
        console.log("ERROR", e.response.data);
        return thunkAPI.rejectWithValue({
          success: e.response.data.success,
          errorCode: e.code,
          errorMessage: e.message,
          message: e.response.data.message,
          status: e.response.data.status,
        } as Response<any>);
      }
    }
  );
  return {
    service: service as AsyncThunk<Response<Res>, Request<Req>, ThunkConfig>,
    reducer: createSlice({
      name: name,
      initialState: {} as Response<Res>,
      reducers: {},
      extraReducers: (build) => {
        build.addCase(service.fulfilled, (_, action) => {
          return {
            success: action.payload.success,
            data: action.payload.data,
            loading: false,
          };
        });
        build.addCase(service.pending, (state) => {
          return {
            data: state.data,
            success: false,
            loading: true,
          };
        });

        build.addCase(service.rejected, (_, action: any) => {
          return {
            errorCode: action.payload.errorCode,
            errorMessage: action.payload.errorMessage,
            status: action.payload.status,
            success: action.payload.success,
            loading: false,
          };
        });
      },
    }).reducer,
  };
};
