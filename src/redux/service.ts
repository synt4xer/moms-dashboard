import { Token } from "@/model/auth";
import { Response } from "@/redux/entity";
import axios, { Method } from "axios";
import { getCookie, setCookie } from "@/utils/cookie";

const ApiService = async <T>({
  endpoint,
  method,
  data,
  baseUrl,
  params,
  useAuth,
}: {
  endpoint: string;
  method?: Method;
  baseUrl?: string;
  data?: any;
  useAuth?: boolean;
  params?: any;
}) => {
  const instance = axios.create({
    ...axios.interceptors,
    baseURL: baseUrl ?? process.env.BASE_URL,
    headers: {
      "x-api-key": process.env.API_KEY,
      Authorization: useAuth
        ? `Bearer ${getCookie("SessionID") ?? ""}`
        : undefined,
    },
  });

  try {
    const response = await instance<T>({
      url: endpoint,
      method: method,
      data: data,
      params: params,
    });

    if (response.status === 200 || response.status === 201) {
      return response.data;
    }
    throw response;
  } catch (e: any) {
    try {
      if (e.response.status === 401) {
        const tokenRes = await instance<Response<Token>>({
          url: "/authentication/refresh",
          method: "POST",
          data: {
            refreshToken: getCookie("SessionRID") ?? "",
          },
        });

        if (tokenRes.status === 201 || tokenRes.status === 200) {
          setCookie("SessionID", tokenRes.data.data?.token ?? "");
          setCookie("SessionRID", tokenRes.data.data?.refreshToken ?? "");
          const response = await instance<T>({
            url: endpoint,
            method: method,
            data: data,
            params: params,
            headers: {
              "x-api-key": process.env.API_KEY,
              Authorization: useAuth
                ? `Bearer ${tokenRes.data.data?.token}`
                : undefined,
            },
          });
          if (response.status === 200 || response.status === 201) {
            return response.data;
          } else {
            throw response;
          }
        }
      }
      throw e;
    } catch (ee) {
      throw ee;
    }
  }
};

export default ApiService;
