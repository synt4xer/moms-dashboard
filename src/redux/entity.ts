import { AppDispatch, RootState } from "./store";

export interface ThunkConfig {
  dispatch: AppDispatch;
  state: RootState;
  rejectValue: Response<any>;
}

export interface Error {
  pesan?: string;
  rc?: string;
  detail?: string;
  message?: string;
}

export interface Response<T = undefined> {
  data?: T;
  loading?: boolean;
  status?: number;
  message?: string;
  success?: boolean;
  errorCode?: string;
  errorMessage?: string;
}

export interface Request<T = undefined> {
  data?: T;
  params?: any;
  additionalPath?: string;
}
