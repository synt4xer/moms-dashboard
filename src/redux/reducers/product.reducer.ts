import { Product } from "@/model/product";
import { Slicer } from "../slicer";
import { List } from "postcss/lib/list";
import { ProductCategory } from "@/model/category.product";

const productListSlice = Slicer<undefined, Product[]>({
  name: "product/list",
  endpoint: "/products/list",
  method: "GET",
  useAuth: true,
});

const productCreateSlice = Slicer<Product, undefined>({
  name: "product/create",
  endpoint: "/products",
  method: "POST",
  useAuth: true,
});

const productByIdSlice = Slicer<undefined, Product>({
  name: "product/getById",
  endpoint: "/products",
  method: "GET",
  useAuth: true,
});

const productUpdateSlice = Slicer<Product, undefined>({
  name: "product/update",
  endpoint: "/products",
  method: "PATCH",
  useAuth: true,
});

const productDeleteSlice = Slicer({
  name: "product/delete",
  endpoint: "/products",
  method: "DELETE",
  useAuth: true,
});

const productCategorySlice = Slicer<undefined, ProductCategory[]>({
  name: "product/category",
  endpoint: "/products/categories",
  method: "GET",
  useAuth: true,
});

export const ProductService = {
  getAll: productListSlice.service,
  getById: productByIdSlice.service,
  update: productUpdateSlice.service,
  create: productCreateSlice.service,
  delete: productDeleteSlice.service,
  getCategories: productCategorySlice.service,
};

const reducer = {
  productList: productListSlice.reducer,
  productCreate: productCreateSlice.reducer,
  productUpdate: productUpdateSlice.reducer,
  productById: productByIdSlice.reducer,
  productDelete: productDeleteSlice.reducer,
  productCategories: productCategorySlice.reducer,
};

export default reducer;
