import { combineReducers } from "@reduxjs/toolkit";
import authReducer from "./auth.reducers";
import productReducer from "./product.reducer";
import userReducer from "./user.reducer";
import voucherReducer from "./voucher.reducer";
import shipmentReducer from "./shipment.reducer";
import tokenReducer from "./token.reducer";

export default combineReducers({
  token: tokenReducer,
  ...authReducer,
  ...productReducer,
  ...userReducer,
  ...voucherReducer,
  ...shipmentReducer,
});
