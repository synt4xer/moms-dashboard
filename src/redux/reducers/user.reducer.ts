import { User } from "@/model/auth";
import { Slicer } from "../slicer";

const userGetAllSlice = Slicer<undefined, User[]>({
  name: "user/all",
  endpoint: "/users",
  method: "GET",
  useAuth: true,
});

const userGetByIdSlice = Slicer<undefined, User>({
  name: "user/getById",
  endpoint: "/users",
  method: "GET",
  useAuth: true,
});

const userCreateSlice = Slicer<User, User>({
  name: "user/create",
  endpoint: "/users",
  method: "POST",
  useAuth: true,
});

const userUpdateSlice = Slicer<User, User>({
  name: "user/update",
  endpoint: "/users",
  method: "PATCH",
  useAuth: true,
});

const userDeleteSlice = Slicer<undefined, undefined>({
  name: "user/delete",
  endpoint: "/users",
  method: "DELETE",
  useAuth: true,
});

export const UserService = {
  getAll: userGetAllSlice.service,
  getById: userGetByIdSlice.service,
  create: userCreateSlice.service,
  update: userUpdateSlice.service,
  delete: userDeleteSlice.service,
};

const reducers = {
  userGetAll: userGetAllSlice.reducer,
  userGetById: userGetByIdSlice.reducer,
  userCreate: userCreateSlice.reducer,
  userUpdate: userUpdateSlice.reducer,
  userDelete: userDeleteSlice.reducer,
};

export default reducers;
