import { Token } from "@/model/auth";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const tokenSlice = createSlice({
  name: "token",
  initialState: {
    token: "",
    refreshToken: "",
  } as Token,
  reducers: {
    setToken: (state, action: PayloadAction<string>) => {
      return {
        ...state,
        token: action.payload,
      };
    },
    setRefreshToken: (state, action: PayloadAction<string>) => {
      return {
        ...state,
        refreshToken: action.payload,
      };
    },
  },
});

export const { setRefreshToken, setToken } = tokenSlice.actions;
export default tokenSlice.reducer;
