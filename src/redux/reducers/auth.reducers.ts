import { Token, User } from "@/model/auth";
import { Slicer } from "../slicer";

const loginSlice = Slicer<User, Token>({
  name: "auth/login",
  endpoint: "/authentication/login",
  method: "POST",
});

const logoutSlice = Slicer<Token, undefined>({
  name: "auth/logout",
  endpoint: "/authentication/logout",
  method: "POST",
});

const registerSlice = Slicer<User, Token>({
  name: "auth/register",
  endpoint: "/authentication/register",
  method: "POST",
});

const refreshSlice = Slicer<User, Token>({
  name: "auth/refresh",
  endpoint: "/authentication/refresh",
  method: "POST",
});

export const AuthService = {
  login: loginSlice.service,
  logout: logoutSlice.service,
  register: registerSlice.service,
  refresh: refreshSlice.service,
};

const reducer = {
  authLogin: loginSlice.reducer,
  authLogout: logoutSlice.reducer,
  authRegister: registerSlice.reducer,
  authRefresh: refreshSlice.reducer,
};

export default reducer;
