import { Voucher } from "@/model/voucher";
import { Slicer } from "../slicer";

const voucherGetAllSlice = Slicer<undefined, Voucher[]>({
  name: "voucher/getall",
  endpoint: "/vouchers",
  method: "GET",
  useAuth: true,
});

export const VoucherService = {
  getAll: voucherGetAllSlice.service,
};

const reducers = {
  voucherGetAll: voucherGetAllSlice.reducer,
};

export default reducers;
