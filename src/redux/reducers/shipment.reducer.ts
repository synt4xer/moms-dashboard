import { Slicer } from "../slicer";
import { Shipment } from "@/model/shipment";

const shipmentGetAllSlice = Slicer<undefined, Shipment[]>({
  name: "shipment/getall",
  endpoint: "/shipments",
  method: "GET",
  useAuth: true,
});


export const ShipmentService = {
  getAll: shipmentGetAllSlice.service,
};

const reducers = {
  shipmemtGetAll: shipmentGetAllSlice.reducer,
};

export default reducers;
