export interface Voucher {
  id: number;
  code: string;
  effect: string;
  activeFrom: Date;
  activeTo: Date;
  quota: number;
  type: string;
  value: string;
  maxValue: string;
  tnc: string;
  isActive: boolean;
  rules: VoucherRule[];
}

export interface VoucherRule {
  id: number;
  nodeType: string;
  nodeId: number;
  key: string;
  operatorFn: string;
  type: string;
  value: string;
  createdAt: Date;
  updatedAt: Date;
}
