export interface User {
  id?: number;
  name?: string;
  email?: string;
  dob?: Date;
  address?: string;
  password?: string;
  phone?: string;
  role?: string;
  isActive?: boolean;
}

export interface Token {
  token: string;
  refreshToken: string;
}
