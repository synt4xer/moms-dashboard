export interface Shipment {
  id?: number;
  code?: string;
  name?: string;
  amount?: string;
  isActive?: boolean;
}
