export interface Product {
  id?: number;
  productCategoryId?: number;
  name?: string;
  categoryName?: string;
  image?: string;
  description?: string;
  price?: string;
  isActive?: boolean;
}
