export interface ProductCategory {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  createdBy: number;
  updatedAt: string;
  updatedBy: number;
  isActive: boolean;
}
