"use client";
import { VscLoading } from "react-icons/vsc";

const Loading = ({ show, size }: { show: boolean; size: number }) => {
  return (
    <div
      className={`relative ${
        show ? "flex" : "hidden"
      } flex-col items-center justify-center`}
      style={{
        height: size,
        width: size,
      }}
    >
      <div
        className={`absolute rounded-full border-slate-200`}
        style={{
          height: size * 0.88,
          width: size * 0.88,
          borderWidth: size * 0.08,
        }}
      />
      <VscLoading
        className={`animate-spin text-violet-500 absolute`}
        style={{
          fontSize: size,
        }}
      />
    </div>
  );
};

export default Loading;
