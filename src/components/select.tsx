import Input from "@/components/input";
import { useEffect, useRef, useState } from "react";
import {
  MdArrowDropDown,
  MdArrowDropDownCircle,
  MdDateRange,
  MdRocket,
  MdSelectAll,
} from "react-icons/md";

const Select = ({
  data,
  keyView,
  selectedDefault,
  onSelected,
  initId,
  idKey,
  label,
}: {
  data: any[];
  idKey: string;
  initId: any;
  label: string;
  selectedDefault?: string;
  keyView: string;
  onSelected: (data: any) => void;
}) => {
  const reff = useRef<HTMLDivElement>(null);
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState<any>();

  useEffect(() => {
    const clickHandler = (event: MouseEvent) => {
      if (
        reff.current &&
        !reff.current.contains(event.target as HTMLDivElement)
      ) {
        setShow(false);
      }
    };
    document.addEventListener("mousedown", clickHandler);
    return () => {
      document.removeEventListener("mousedown", clickHandler);
    };
  });

  useEffect(() => {
    if (selectedDefault) {
    }
  }, [selectedDefault]);

  useEffect(() => {
    const dt = data.filter((e) => e[idKey] === initId);
    if (dt.length > 0) {
      const el = dt[0];
      setSelected(el);
    }
  }, [data]);

  return (
    <div className="w-full relative">
      <Input
        label={label}
        viewOnly={false}
        leading={
          <div className="w-[56px] h-full flex flex-col items-center text-[28px] justify-center bg-violet-600 text-white">
            <MdSelectAll />
          </div>
        }
        config={{
          name: label.toLowerCase().replaceAll(" ", "_"),
          type: "text",
          value: selected ? selected[keyView] : "",
          lang: "id-ID",
          placeholder: `Select ${label}`,
        }}
        trailing={
          <div
            onClick={() => setShow(!show)}
            className="text-[24px] cursor-pointer text-violet-500 px-3 h-full flex flex-col items-center justify-center"
          >
            <MdArrowDropDownCircle />
          </div>
        }
      />
      <div
        ref={reff}
        className={`bg-white w-full shadow-2xl absolute border-slate-100 border-[0.7px] overflow-hidden bottom-[52px] flex flex-col items-center divide-y-[1px] divide-violet-100 rounded-lg transition-all ease-in-out duration-700`}
      >
        {show && (
          <div className="px-4 py-3 w-full text-center text-violet-600 font-bold">
            {`Select ${label}`}
          </div>
        )}
        {show && (
          <div className="max-h-[300px] overflow-auto w-full">
            {data.map((e) => (
              <div
                key={e[keyView]}
                onClick={() => {
                  onSelected(e);
                  setSelected(e);
                  setShow(false);
                }}
                className="px-4 py-3 w-full hover:bg-violet-600 hover:text-white"
              >
                {e[keyView]}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Select;
