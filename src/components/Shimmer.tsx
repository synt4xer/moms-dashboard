"use client";
const Shimmer = ({ w, h }: { w: string; h: string }) => {
  return (
    <div className="animate-pulse">
      <div className={`${w} ${h} bg-slate-300 rounded`} />
    </div>
  );
};

export default Shimmer;
