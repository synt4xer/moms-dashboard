import { ReactNode } from "react";
import { MdClose } from "react-icons/md";

const Dialog = ({
  title,
  show,
  onClose,
  bg,
  maxW,
  children,
}: {
  title?: string;
  bg?: string;
  maxW?: string;

  show: boolean;
  onClose?: () => void;
  children: ReactNode;
}) => {
  return (
    <div
      className={`bg-black/10 fixed w-full top-0 bottom-0 left-0 right-0 ${
        show ? "opacity-100 z-[99999] backdrop-blur-sm" : "opacity-0 -z-[10]"
      } transition-all ease-in-out duration-1000 flex flex-col p-4 items-center justify-center`}
    >
      <div
        className={`${bg ?? "bg-white"} ${
          maxW ?? "max-w-md"
        } w-full min-w-sm min-h-[100px] relative rounded-3xl ${
          show ? "scale-100 opacity-100" : "scale-75 opacity-0"
        } transition-all ease-in-out flex flex-col items-center justify-start relative duration-700 overflow-hidden`}
      >
        {(title || onClose) && (
          <div className="flex flex-row items-center p-2 w-full border-b-[0.7px] border-b-violet-100">
            {title ? (
              <p className="flex-1 px-4 text-lg text-violet-600 font-bold">
                {title}
              </p>
            ) : (
              <div className="flex-1" />
            )}
            {onClose && (
              <div
                onClick={onClose}
                className="h-[46px] w-[46px] cursor-pointer rounded-full bg-pink-600 flex flex-col items-center justify-center text-white text-[32px]"
              >
                <MdClose />
              </div>
            )}
          </div>
        )}
        <div className="overflow-auto h-full relative w-full flex flex-col items-start justify-start">
          <div className="w-full">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Dialog;
