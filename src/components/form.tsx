"use client";
import { ReactNode } from "react";

const Form = ({
  children,
  onSubmit,
  className,
}: {
  children: ReactNode;
  onSubmit: (_e: any) => void;
  className?: string | undefined;
}) => {
  return (
    <form
      method="POST"
      noValidate={true}
      autoComplete="false"
      onSubmit={async (e) => {
        let isValid: boolean[] = [];
        let isChecked: boolean = false;
        const dataObj: string[] = [];
        let pins: HTMLInputElement[] = [];
        e.preventDefault();
        const element = e.target as HTMLElement;
        const dataInput: HTMLInputElement[] = [].slice.call(
          element.getElementsByTagName("input")
        );
        dataInput.forEach((e) => {
          if (e.type !== "submit" && e.type !== "file") {
            if (e.value) {
              const clearSpaceValue = removeSpace(e.value);
              e.value = clearSpaceValue;
              if (e.value.length > 0) {
                dataObj.push(`"${e.name}":"${e.value}"`);
                if (validateLength(e)) {
                  switch (e.type) {
                    case "email":
                      isChecked = validateEmail(e);
                      break;
                    case "password":
                      isChecked = validatePINNumberOnly(e);
                      if (isChecked) {
                        if (e.name.toLocaleLowerCase().includes("pin")) {
                          pins.push(e);
                        }
                      }
                      break;
                    case "text":
                      isChecked = validateName(e);
                      if (e.name.toLowerCase().includes("pin")) {
                        isChecked = validatePINNumberOnly(e);
                        if (isChecked) {
                          if (e.name.toLocaleLowerCase().includes("pin")) {
                            pins.push(e);
                          }
                        }
                      }
                      break;
                    case "number":
                      isChecked = true;
                      break;
                    default:
                      isChecked = true;
                      setError("", e);
                  }
                } else {
                  isChecked = false;
                }
              } else {
                setError(`${e.title} can't be empty`, e);
                isChecked = false;
              }
            } else {
              setError(`${e.title} can't be empty`, e);
              isChecked = false;
            }
            setChecked(isChecked, e);
            isValid.push(isChecked);
          }
        });

        // validate pin
        if (pins.length > 1) {
          const firstPin = pins[0];
          for (let i = 0; i < pins.length; i++) {
            if (pins[i].value !== firstPin.value) {
              setChecked(false, pins[i]);
              setChecked(false, firstPin);
              setError("PIN tidak sama", pins[i]);
              setError("PIN tidak sama", firstPin);
              isValid.push(false);
            } else {
              isValid.push(true);
            }
          }
        }

        if (isValid.filter((e) => e === false).length === 0) {
          onSubmit(JSON.parse(`{${dataObj.toString()}}`));
        }
      }}
      className={`px-6 flex items-center my-4 flex-col space-y-4 md:px-8 ${className}`}
    >
      {children}
    </form>
  );
};

const validateEmail = (ee: HTMLInputElement) => {
  const expression: RegExp = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
  if (expression.test(ee.value)) {
    setError("", ee);
    return true;
  } else {
    setError("Invalid email", ee);
    return false;
  }
};

export const validatePINNumberOnly = (ee: HTMLInputElement) => {
  if (ee.name.toLowerCase().includes("pin")) {
    const expression: RegExp = /^\d+$/;
    if (expression.test(ee.value)) {
      return validateLength(ee);
    } else {
      setError("PIN must be number 0-9", ee);
      return false;
    }
  } else {
    setError("", ee);
    return true;
  }
};

export const validateName = (ee: HTMLInputElement) => {
  if (ee.name.toLowerCase().includes("name")) {
    const expression: RegExp = /^[a-zA-Z ]*$/;
    if (expression.test(ee.value)) {
      return validateLength(ee);
    } else {
      setError("Nama terdiri dari A-Z atau a-z", ee);
      return false;
    }
  } else {
    setError("", ee);
    return true;
  }
};

export const validateAutoComplete = (ee: HTMLInputElement) => {
  if (ee.name.includes("phone")) {
    const reVal = ee.value.replace(/^(?:0+(?=[1-9])|0+(?=0$))/gm, "");
    ee.className =
      "inputNumber flex-1 text-lg text-slate-700 active:decoration-transparent focus:decoration-transparent focus:outline-none active:outline-none";
    ee.value = reVal.substring(0, ee.maxLength);
  }

  if (ee.name.includes("name")) {
    const newValue = ee.value.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
    ee.value = newValue;
  }
};

function removeSpace(s: string) {
  s = s.replace(/(^\s*)|(\s*$)/gi, "");
  s = s.replace(/[ ]{2,}/gi, " ");
  s = s.replace(/\n /, "\n");
  return s;
}

const validateLength = (ee: HTMLInputElement) => {
  if (ee.value.length < ee.minLength && ee.minLength > 0) {
    setError(`Minimum ${ee.minLength} character`, ee);
    return false;
  } else if (ee.value.length > ee.maxLength && ee.maxLength > 0) {
    setError(`Maximum ${ee.maxLength} character`, ee);
    return false;
  } else {
    setError("", ee);
    return true;
  }
};

export const setError = (message: string, ee?: HTMLElement) => {
  if (ee) {
    const el = ee?.parentElement as HTMLDivElement;
    if (el) {
      const lastEl = el.parentElement?.lastElementChild;
      if (lastEl) {
        const innerEl = lastEl.lastElementChild;
        const innerFirstEl = lastEl.firstElementChild;
        if (message.length === 0) {
          if (innerFirstEl) {
            innerFirstEl.className = "hidden";
            lastEl.className = "hidden";
          }
        } else {
          if (innerEl) {
            innerEl.innerHTML = message;
          }

          if (innerFirstEl) {
            innerFirstEl.className = "mr-2 text-md text-red-500";
            lastEl.className =
              "flex flex-row text-[10pt] items-center text-red-500 font-medium justify-end";
          }
        }
      }
    }
  }
};

export const setChecked = (isChecked: boolean, ee?: HTMLInputElement) => {
  if (ee) {
    const el = ee?.parentElement as HTMLDivElement;
    if (el) {
      if (isChecked) {
        const spansEl: HTMLSpanElement[] = [].slice.call(
          el.getElementsByTagName("span")
        );
        spansEl[0].className = "text-green-500 text-[18px] p-2";
      } else {
        const spansEl: HTMLSpanElement[] = [].slice.call(
          el.getElementsByTagName("span")
        );
        spansEl[0].className = "hidden";
      }
    }
  }
};

export default Form;
