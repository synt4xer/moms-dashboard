import { FcDeleteDatabase } from "react-icons/fc";
import Loading from "./loading";

const DataNotFound = ({ loading }: { loading?: boolean }) => {
  return (
    <div className="w-full flex-2 h-full flex flex-col items-center justify-center">
      {loading ? (
        <Loading show={true} size={64} />
      ) : (
        <>
          <FcDeleteDatabase size={64} />
          <p className="text-[18px] text-slate-600">No Data</p>
        </>
      )}
    </div>
  );
};

export default DataNotFound;
