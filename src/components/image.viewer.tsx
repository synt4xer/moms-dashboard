import Image from "next/image";
import Dialog from "./dialog";
import { MdClose, MdImage } from "react-icons/md";
import { useEffect, useState } from "react";

const ImageViewer = ({
  src,
  show,
  onClose,
}: {
  show: boolean;
  src: string;
  onClose?: () => void;
}) => {
  const [image, setImage] = useState("");
  useEffect(() => {
    if (show) {
      setImage(src);
    } else {
      setTimeout(() => {
        setImage("");
      }, 700);
    }
  }, [show]);

  return (
    <div
      className={`bg-white/10 fixed w-full top-0 bottom-0 left-0 right-0 ${
        show ? "opacity-100 z-[99999] backdrop-blur-md" : "opacity-0 -z-[10]"
      } transition-all ease-in-out duration-1000 flex flex-col p-8 items-center justify-center`}
    >
      <div
        className={`${
          show ? "opacity-100 scale-100" : "opacity-0 scale-50"
        } relative h-full w-full flex flex-col items-center justify-center transition-all ease-in-out duration-700 overflow-hidden`}
      >
        {image.length > 5 &&
        (image.includes("http://") ||
          image.includes("https://") ||
          image.includes(".")) ? (
          <Image alt="image" fill={true} src={image} objectFit="contain" />
        ) : (
          <>
            <MdImage className="text-[220px] text-pink-500" />
            <p className="text-[24px] md:text-[32px] font-bold text-violet-600">
              {`<-- Invalid Image URL -->`}
            </p>
          </>
        )}
      </div>
      <div
        onClick={onClose}
        className="h-[46px] w-[46px] absolute top-[24px] right-[24px] z-2 cursor-pointer rounded-full bg-pink-600 flex flex-col items-center justify-center text-white text-[32px]"
      >
        <MdClose />
      </div>
    </div>
  );
};

export default ImageViewer;
