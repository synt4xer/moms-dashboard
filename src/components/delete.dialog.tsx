import Dialog from "@/components/dialog";
import Loading from "@/components/loading";
import { Response } from "@/redux/entity";
import ApiService from "@/redux/service";
import { useEffect, useState } from "react";

const DeleteDialog = ({
  show,
  content,
  path,
  onClose,
  title,
}: {
  onClose?: () => void;
  show: boolean;
  content: string;
  path: string;
  title: string;
}) => {
  const [reqState, setReqState] = useState<{
    loading: boolean;
    success: boolean;
  }>({
    loading: false,
    success: false,
  });

  useEffect(() => {
    if (show) {
      setReqState({
        loading: false,
        success: false,
      });
    } else {
      setTimeout(() => {
        setReqState({
          loading: false,
          success: false,
        });
      }, 700);
    }
  }, [show]);

  return (
    <Dialog maxW="max-w-md" show={show} title={title} onClose={onClose}>
      <div className="w-full h-[200px] flex flex-col items-center justify-center">
        {reqState.loading ? (
          <Loading show={true} size={64} />
        ) : reqState.success ? (
          <h2 className="text-xl font-medium text-teal-600">{`Success delete!`}</h2>
        ) : (
          <div className="w-full space-y-2 flex flex-col items-center justify-center p-4">
            <p>Are you sure to delete this</p>
            <h2 className="text-xl pb-8 font-medium text-violet-600">
              {content}
            </h2>
            <button
              className="w-full max-w-[200px] py-3 rounded-lg bg-pink-600 text-white text-lg"
              onClick={async () => {
                setReqState({
                  success: false,
                  loading: true,
                });

                const response = await ApiService<Response<undefined>>({
                  endpoint: path,
                  method: "DELETE",
                  useAuth: true,
                });
                setReqState({
                  success: response.success ?? false,
                  loading: false,
                });
              }}
            >
              Delete
            </button>
          </div>
        )}
      </div>
    </Dialog>
  );
};

export default DeleteDialog;
