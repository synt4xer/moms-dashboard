"use client";
import { DetailedHTMLProps, InputHTMLAttributes, ReactNode } from "react";
import { IoCheckmarkCircle } from "react-icons/io5";
import { MdError } from "react-icons/md";
import { setError, validateAutoComplete } from "./form";

const Input = ({
  label,
  leading,
  trailing,
  maxWidth,
  viewOnly,
  onClick,
  config,
}: {
  label?: string;
  leading?: ReactNode;
  maxWidth?: string;
  onClick?: () => void;
  viewOnly?: boolean;
  trailing?: ReactNode;
  config: DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
}) => {
  return (
    <div
      className={`${
        config.type === "submit"
          ? ""
          : `flex flex-col ${maxWidth ?? "w-full"} my-2`
      }`}
    >
      {label && <label className="text-md mb-2 text-slate-500">{label}</label>}
      <div
        onClick={onClick}
        className={`${
          config.type === "submit"
            ? ""
            : "flex flex-row h-[46px] relative items-center justify-start rounded-lg border-[1px] border-slate-300/50 overflow-hidden"
        }`}
      >
        {leading}
        <input
          className={`flex-1 px-4 min-w-0 w-full h-full text-md text-slate-700 active:decoration-transparent focus:decoration-transparent focus:outline-none active:outline-none ${
            maxWidth ?? "w-full"
          } ${
            viewOnly ? "bg-white" : config.disabled ? "bg-slate-50" : "bg-white"
          } `}
          onInput={(e) => {
            setError("", e.target as HTMLElement);
            validateAutoComplete(e.target as HTMLInputElement);
          }}
          title={label}
          placeholder={`Input ${label}`}
          disabled={viewOnly}
          {...config}
        />
        {config.type !== "submit" && (
          <span className="hidden">
            <IoCheckmarkCircle />
          </span>
        )}
        {trailing && <div className="w-[1px] bg-slate-200 h-full" />}
        {trailing}
      </div>
      <div className="hidden">
        <span className="hidden">
          <MdError />
        </span>
        <p className="py-1 italic text-end"></p>
      </div>
    </div>
  );
};
export default Input;
