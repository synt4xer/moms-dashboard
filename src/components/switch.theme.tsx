"use client";
import { useEffect, useState } from "react";

const SwitchTheme = ({ size }: { size: number }) => {
  const [isDark, setIsDark] = useState(
    localStorage.getItem("theme") === "dark"
  );
  const wd = (2 * size + 8).toFixed() + "px";

  useEffect(() => {
    if (
      localStorage.getItem("theme") === "dark" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: dark)").matches)
    ) {
      setIsDark(true);
      document.documentElement.classList.add("dark");
      localStorage.setItem("theme", "dark");
    } else {
      setIsDark(false);
      document.documentElement.classList.remove("dark");
      localStorage.setItem("theme", "light");
    }
  }, []);

  const switchThemes = () => {
    if (!isDark) {
      setIsDark(true);
      document.documentElement.classList.add("dark");
      localStorage.setItem("theme", "dark");
    } else {
      setIsDark(false);
      document.documentElement.classList.remove("dark");
      localStorage.setItem("theme", "light");
    }
  };

  return (
    <div className="flex flex-col items-center">
      <div
        className={`cursor-pointer bg-slate-200 dark:bg-slate-600 p-[4px] rounded-full w-[72px] flex flex-row items-center transition-colors ease-in-out duration-700`}
        onClick={() => switchThemes()}
      >
        <div
          className={`h-[32px] w-[32px] bg-yellow-500 dark:bg-cyan-600 rounded-full
          ${
            isDark ? "translate-x-full" : "tanslate-x-0"
          } transition-transform ease-in-out duration-500`}
        ></div>
      </div>
    </div>
  );
};

export default SwitchTheme;
