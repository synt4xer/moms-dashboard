"use client";
import Form from "@/components/form";
import Input from "@/components/input";
import Loading from "@/components/loading";
import { AuthService } from "@/redux/reducers/auth.reducers";
import { setRefreshToken, setToken } from "@/redux/reducers/token.reducer";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { useEffect } from "react";
import { MdArrowForward } from "react-icons/md";
import Logo from "./dashboard/component/logo";
import { setCookie } from "@/utils/cookie";

export default function Home() {
  const dispatch = useAppDispatch();
  const { data, success, loading } = useAppSelector((state) => state.authLogin);

  useEffect(() => {
    if (success && data) {
      dispatch(setToken(data.token ?? ""));
      dispatch(setRefreshToken(data.refreshToken ?? ""));
      setCookie("SessionID", data.token);
      setCookie("SessionRID", data.refreshToken);
      window.location.reload();
    }
  }, [success, data, dispatch]);

  return (
    <>
      <head>
        <title>Mooms - Login</title>
      </head>
      <main className="fixed top-0 bottom-0 w-full flex flex-col items-center justify-center">
        <div className="max-w-[300px] w-full">
          <Logo />
        </div>
        <Form
          className="w-full max-w-sm space-y-4"
          onSubmit={(e) => {
            dispatch(
              AuthService.login({
                data: {
                  email: e.email,
                  password: e.password,
                },
              })
            );
          }}
        >
          <Input
            label="Email"
            config={{
              type: "email",
              name: "email",
            }}
          />
          <Input
            label="Password"
            config={{
              type: "password",
              role: "presentation",
              name: "password",
              autoComplete: "new-password",
            }}
          />
          <div className="w-full flex justify-between pl-4 py-3 flex-row items-center">
            <p className="text-2xl font-bold text-violet-600">Sign In</p>
            <div className="relative flex flex-col w-[68px] h-[68px] items-center justify-center">
              <Loading show={loading ?? false} size={68} />
              <button
                disabled={loading}
                className="p-2 absolute z-2  bg-violet-600 text-white rounded-full"
              >
                <MdArrowForward size={32} />
              </button>
            </div>
          </div>
        </Form>
      </main>
    </>
  );
}
