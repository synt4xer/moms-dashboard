"use client";
import { Inter } from "next/font/google";
import "./globals.css";
import { Provider } from "react-redux";
import { psStore, store } from "@/redux/store";
import { PersistGate } from "redux-persist/integration/react";

const inter = Inter({ subsets: ["greek"] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className + " text-slate-700"}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={psStore}>
            {children}
          </PersistGate>
        </Provider>
      </body>
    </html>
  );
}
