"use client";

import { MdDelete, MdEdit, MdVisibility } from "react-icons/md";
import ContentLayout from "../content";
import { VoucherService } from "@/redux/reducers/voucher.reducer";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { useEffect } from "react";
import DataNotFound from "@/components/data.notfound";
import { getDateWWDDMMYYY } from "@/utils/converter";

const page = () => {
  const dispatch = useAppDispatch();
  const { data, loading } = useAppSelector((state) => state.voucherGetAll);
  useEffect(() => {
    dispatch(VoucherService.getAll({}));
  }, [dispatch]);

  return (
    <ContentLayout pageTitle="Vouchers">
      <div className="w-full h-[160px] sticky left-0 bg-amber-100"></div>
      {data && data.length > 0 ? (
        <table className="table-responsive w-full">
          <thead className="bg-violet-600 font-bold text-center sticky top-[68px] left-0 text-white">
            <tr>
              <td className="px-3 py-3 min-w-[300px] whitespace-nowrap">
                Name
              </td>
              <td className="px-3 py-3 min-w-[200px] whitespace-nowrap">
               Active From
              </td>
              <td className="px-3 py-3 whitespace-nowrap min-w-[200px]">Active To</td>
              <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                Quota
              </td>
              <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                Status
              </td>
              <td className="px-3 py-3 whitespace-nowrap">Actions</td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((e, i) => (
                <tr
                  key={e.id}
                  className={`${
                    i % 2 === 0 ? "bg-white" : "bg-slate-100/70"
                  } hover:bg-violet-200`}
                >
                  <td className="px-3 py-3">{e.code}</td>
                  <td className="px-3 py-3">
                    {getDateWWDDMMYYY(new Date(e.activeFrom), "id-ID")}
                  </td>
                  <td className="px-3 py-3 text-center">
                    {getDateWWDDMMYYY(new Date(e.activeTo), "id-ID")}
                  </td>
                  <td className="pl-3 pr-6 py-3 text-end">{e.quota}</td>
                  <td className="px-3 py-3 text-center">
                    {e.isActive ? "ACTIVE" : "INCATIVE"}
                  </td>
                  <td className="flex py-3 px-4 flex-row text-slate-700 text-[24px] items-center justify-center space-x-6">
                    <MdVisibility className="text-teal-500 cursor-pointer" />
                    <MdEdit className="text-amber-500 cursor-pointer" />
                    <MdDelete className="text-red-500 cursor-pointer" />
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      ) : (
        <DataNotFound loading={loading} />
      )}
    </ContentLayout>
  );
};

export default page;
