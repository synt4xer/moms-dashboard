import Dialog from "@/components/dialog";
import Form from "@/components/form";
import Input from "@/components/input";
import Loading from "@/components/loading";
import Select from "@/components/select";
import { ProductCategory } from "@/model/category.product";
import { Product } from "@/model/product";
import { Response } from "@/redux/entity";
import { ProductService } from "@/redux/reducers/product.reducer";
import ApiService from "@/redux/service";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import {
  MdAddAPhoto,
  MdCheck,
  MdCheckBox,
  MdCheckBoxOutlineBlank,
  MdEditDocument,
  MdPriceChange,
} from "react-icons/md";

const UpdateProductDialog = ({
  show,
  data,
  onClose,
}: {
  onClose?: () => void;
  data?: Product;
  show: boolean;
}) => {
  const inputFile = useRef<HTMLInputElement | null>(null);
  const [image, setImage] = useState("");
  const [product, setProduct] = useState(data);
  const [reqState, setReqState] = useState<{
    hidden: boolean;
    loading: boolean;
    success: boolean;
  }>({
    hidden: true,
    loading: false,
    success: false,
  });
  const initData = data;
  const [blobImage, setBlobImage] = useState<Blob>();
  const [categoryId, setCategoryId] = useState(data?.productCategoryId);
  const [enableButton, setEnableButton] = useState(false);
  const dispatch = useAppDispatch();
  const { data: categories } = useAppSelector(
    (state) => state.productCategories
  );

  useEffect(() => {
    if (show) {
      setReqState({
        hidden: !show,
        loading: false,
        success: false,
      });
      setProduct(data);
      console.log("IMAGE URL", data?.image);
      fetch(data?.image ?? "")
        .then((response) => response.blob())
        .then((blob) => {
          console.log("IMAGE", blob);
          setImage(URL.createObjectURL(blob));
          setBlobImage(blob);
        });
    } else {
      setTimeout(() => {
        setCategoryId(undefined);
        setBlobImage(undefined);
        setImage("");
        setProduct(undefined);
        setEnableButton(false);
        setReqState({
          hidden: true,
          loading: false,
          success: false,
        });
      }, 700);
    }
  }, [show]);

  useEffect(() => {
    if (JSON.stringify(product) !== JSON.stringify(data)) {
      setEnableButton(true);
    } else {
      setEnableButton(false);
    }
    console.log("BERUBAH INI!!!", initData);
    console.log("BERUBAH PRO!!!", product);
  }, [product]);

  useEffect(() => {
    dispatch(ProductService.getCategories({}));
  }, []);
  return (
    <Dialog maxW="max-w-md" show={show} title="Add Product" onClose={onClose}>
      {!reqState.hidden && (
        <Form
          onSubmit={async (e) => {
            setReqState({
              hidden: true,
              success: false,
              loading: true,
            });
            const form = new FormData();
            form.append("name", e.name);
            form.append("description", e.description);
            form.append("productCategoryId", `${product?.productCategoryId}`);
            form.append("price", e.price);
            form.append("isActive", `${product?.isActive}`);
            if (blobImage) {
              form.append(
                "image",
                blobImage ?? new Blob([], { type: "image/png" })
              );
            } else {
              form.append("image", `${product?.image}`);
            }

            const response = await ApiService<Response<undefined>>({
              endpoint: "/products/" + product?.id,
              method: "PATCH",
              data: form,
              useAuth: true,
            });
            if (response.status === 201 || response.status === 200) {
              console.log("DATA", response);
              setReqState({
                hidden: true,
                success: true,
                loading: false,
              });
            } else {
              setReqState({
                hidden: true,
                success: false,
                loading: false,
              });
            }
          }}
        >
          <div
            onClick={() => inputFile.current?.click()}
            className="h-[200px] cursor-pointer active:scale-75 transition-all ease-in-out duration-500 bg-slate-100 flex flex-col items-center justify-center w-full rounded-md overflow-hidden relative"
          >
            {product?.image ? (
              <Image
                fill={true}
                objectFit="contain"
                src={image ?? ""}
                alt="upload"
              />
            ) : (
              <MdAddAPhoto className="text-[64px] text-violet-500" />
            )}
          </div>
          <input
            id="upload"
            type="file"
            ref={inputFile}
            accept="image/png, image/jpg, image/jpeg"
            onChange={(event) => {
              if (event.target.files) {
                let fg = event.target.files[0];
                if (fg && fg.type) {
                  fg.arrayBuffer().then((arrayBuffer) => {
                    const blob = new Blob([new Uint8Array(arrayBuffer)], {
                      type: fg.type,
                    });
                    setBlobImage(blob);
                    setImage(URL.createObjectURL(blob));
                  });
                }
              }
            }}
            style={{
              display: "none",
            }}
          />
          <Input
            label="Name"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdEditDocument />
              </div>
            }
            config={{
              name: "name",
              value: product?.name,
              onChange: (val) => {
                const p = {
                  ...product,
                  name: val.target.value,
                };
                setProduct(p);
              },
            }}
          />
          <Input
            label="Description"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdEditDocument />
              </div>
            }
            config={{
              name: "description",
              type: "text",
              value: product?.description,
              onChange: (val) => {
                const p = {
                  ...product,
                  description: val.target.value,
                };
                setProduct(p);
              },
            }}
          />
          <Select
            idKey="id"
            initId={product?.productCategoryId}
            label="Category"
            data={categories ?? []}
            keyView="name"
            onSelected={(data: ProductCategory) => {
              const p = {
                ...product,
                categoryName: data.name,
                productCategoryId: data.id,
              };
              setProduct(p);
              setCategoryId(data.id);
            }}
          />
          <Input
            label="Price"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdPriceChange />
              </div>
            }
            config={{
              name: "price",
              type: "number",
              value: product?.price,
              onChange: (val) => {
                const p = {
                  ...product,
                  price: val.target.value,
                };
                setProduct(p);
              },
            }}
          />
          <CheckList
            isActive={data?.isActive ?? false}
            onChange={(val) => {
              const p = {
                ...product,
                isActive: val,
              };
              setProduct(p);
            }}
          />
          <div className="p-4 w-full flex flex-col items-center">
            <button
              disabled={!enableButton}
              className={`px-4 max-w-sm w-full py-3 active:opacity-90 rounded-lg ${
                enableButton
                  ? "bg-violet-600 text-white"
                  : "bg-slate-300 text-slate-500"
              }`}
            >
              Submit
            </button>
          </div>
        </Form>
      )}
      {reqState.loading && (
        <div className="w-full h-[300px] flex flex-col items-center justify-center">
          <Loading show={true} size={64} />
        </div>
      )}
      {reqState.success && (
        <div className="w-full h-[300px] text-slate-700 flex flex-col items-center justify-center">
          <p>Success Add Product</p>
        </div>
      )}
    </Dialog>
  );
};

const CheckList = ({
  isActive,
  onChange,
}: {
  isActive: boolean;
  onChange: (active: boolean) => void;
}) => {
  const [active, setActive] = useState(isActive);
  return (
    <div className="flex flex-col items-start w-full">
      <p className="text-slate-500 py-2">Status</p>
      <div
        onClick={() => {
          setActive(!active);
          onChange(!active);
        }}
        className="flex w-full flex-row h-[46px] relative items-center justify-start rounded-lg border-[1px] border-slate-300/50 overflow-hidden"
      >
        <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
          <MdCheck />
        </div>
        <p className="flex-1 px-4">{active ? "ACTIVE" : "DISABLED"}</p>{" "}
        <div className="text-[32px] text-violet-600 px-2 py-2">
          {active ? <MdCheckBox /> : <MdCheckBoxOutlineBlank />}
        </div>
      </div>
    </div>
  );
};

export default UpdateProductDialog;
