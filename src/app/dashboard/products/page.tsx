"use client";

import {
  MdAdd,
  MdDelete,
  MdEdit,
  MdImage,
  MdSearch,
  MdVisibility,
} from "react-icons/md";
import ContentLayout from "../content";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { useEffect, useState } from "react";
import { ProductService } from "@/redux/reducers/product.reducer";
import Input from "@/components/input";
import { VscLoading } from "react-icons/vsc";
import { formatToIDR } from "@/utils/converter";
import ImageViewer from "@/components/image.viewer";
import DataNotFound from "@/components/data.notfound";
import AddProductDialog from "./add.product";
import UpdateProductDialog from "./update.product";
import { Product } from "@/model/product";
import DeleteDialog from "@/components/delete.dialog";

const page = () => {
  const dispatch = useAppDispatch();
  const [seacrhValue, setSearchValue] = useState("");
  const [selectedProduct, setSelectedProduct] = useState<Product>();
  const [showImage, setShowImage] = useState({
    show: false,
    src: "",
  });
  const [showAddProduct, setShowAddProduct] = useState(false);
  const [showDeleteProduct, setShowDeleteProduct] = useState({
    path: "",
    content: "",
    show: false,
  });
  const { data, loading } = useAppSelector((state) => state.productList);
  useEffect(() => {
    let timeout = setTimeout(() => {
      dispatch(
        ProductService.getAll({
          params: {
            name: seacrhValue,
          },
        })
      );
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [seacrhValue, dispatch]);
  return (
    <>
      <ContentLayout pageTitle="Product">
        <div className="w-full flex flex-row items-end justify-between sticky p-4 left-0 bg-white">
          <div>
            <Input
              maxWidth="max-w-xs"
              config={{
                value: seacrhValue,
                autoComplete: "no",
                placeholder: "Search name...",
                onChange: (e) => {
                  setSearchValue(e.target.value);
                },
              }}
              trailing={
                <div className="flex cursor-pointer active:opacity-60 flex-row items-center justify-center w-[56px] h-full text-center text-[32px] bg-violet-600 text-white">
                  {loading ? (
                    <VscLoading className="animate-spin" />
                  ) : (
                    <MdSearch />
                  )}
                </div>
              }
            />
          </div>
          <div className="flex flex-row items-center space-x-2">
            <div
              onClick={() => setShowAddProduct(true)}
              className="flex px-3 py-3 transition-all ease-in-out duration-700 active:scale-75 active:opacity-65 cursor-pointer flex-row items-center space-x-2"
            >
              <p className="text-slate-600 font-medium">Add Product</p>
              <MdAdd className="text-[36px] p-2 text-white rounded-full bg-violet-600" />
            </div>
          </div>
        </div>
        {data && data.length > 0 ? (
          <table className="table-responsive w-full">
            <thead className="bg-violet-600 font-bold text-center sticky top-[68px] left-0 text-white">
              <tr>
                <td className="px-3 py-3 min-w-[300px] whitespace-nowrap">
                  Name
                </td>
                <td className="px-3 py-3 min-w-[600px] whitespace-nowrap">
                  Description
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[160px]">
                  Category Name
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                  Price
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                  Status
                </td>
                <td className="px-3 py-3 whitespace-nowrap">Actions</td>
              </tr>
            </thead>
            <tbody>
              {data &&
                data.map((e, i) => (
                  <tr
                    key={e.id}
                    className={`${
                      i % 2 === 0 ? "bg-white" : "bg-slate-100/70"
                    } hover:bg-violet-200`}
                  >
                    <td className="px-3 py-3">{e.name}</td>
                    <td className="px-3 py-3">{e.description}</td>
                    <td className="px-3 py-3 text-center">{e.categoryName}</td>
                    <td className="pl-3 pr-6 py-3 text-end">
                      {formatToIDR(parseInt(e.price ?? "0"))}
                    </td>
                    <td className="px-3 py-3 text-center">
                      {e.isActive ? "ACTIVE" : "INCATIVE"}
                    </td>
                    <td className="flex py-3 px-4 flex-row text-slate-700 text-[24px] items-center justify-center space-x-6">
                      <MdImage
                        className="text-pink-600 cursor-pointer"
                        onClick={() =>
                          setShowImage({
                            show: true,
                            src: e.image ?? "",
                          })
                        }
                      />
                      <MdEdit
                        onClick={() => setSelectedProduct(e)}
                        className="text-amber-500 cursor-pointer"
                      />
                      <MdDelete
                        onClick={() =>
                          setShowDeleteProduct({
                            path: "/products/" + e.id,
                            content: `${e.name}`,
                            show: true,
                          })
                        }
                        className="text-red-500 cursor-pointer"
                      />
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        ) : (
          <DataNotFound loading={loading} />
        )}
      </ContentLayout>
      <ImageViewer
        onClose={() => {
          setShowImage({
            src: "",
            show: false,
          });
        }}
        show={showImage.show}
        src={showImage.src}
      />
      <AddProductDialog
        show={showAddProduct}
        onClose={() => {
          dispatch(
            ProductService.getAll({
              params: {
                name: "",
              },
            })
          );
          setShowAddProduct(false);
        }}
      />
      <UpdateProductDialog
        show={selectedProduct ? true : false}
        onClose={() => {
          dispatch(
            ProductService.getAll({
              params: {
                name: "",
              },
            })
          );
          setSelectedProduct(undefined);
        }}
        data={selectedProduct}
      />
      <DeleteDialog
        title="Delete Product"
        path={showDeleteProduct.path}
        show={showDeleteProduct.show}
        onClose={() => {
          dispatch(
            ProductService.getAll({
              params: {
                name: "",
              },
            })
          );
          setShowDeleteProduct({
            path: "",
            content: "",
            show: false,
          });
        }}
        content={showDeleteProduct.content}
      />
    </>
  );
};

export default page;
