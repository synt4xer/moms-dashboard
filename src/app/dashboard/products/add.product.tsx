import Dialog from "@/components/dialog";
import Form from "@/components/form";
import Input from "@/components/input";
import Loading from "@/components/loading";
import Select from "@/components/select";
import { ProductCategory } from "@/model/category.product";
import { Response } from "@/redux/entity";
import { ProductService } from "@/redux/reducers/product.reducer";
import ApiService from "@/redux/service";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { MdAddAPhoto, MdEditDocument, MdPriceChange } from "react-icons/md";

const AddProductDialog = ({
  show,
  onClose,
}: {
  onClose?: () => void;
  show: boolean;
}) => {
  const inputFile = useRef<HTMLInputElement | null>(null);
  const [image, setImage] = useState("");
  const [reqState, setReqState] = useState<{
    hidden: boolean;
    loading: boolean;
    success: boolean;
  }>({
    hidden: true,
    loading: false,
    success: false,
  });
  const [success, setSuccess] = useState(false);
  const [blobImage, setBlobImage] = useState<Blob>();
  const [selectedCategory, setSelectedCategory] = useState<ProductCategory>();
  const dispatch = useAppDispatch();
  const { data } = useAppSelector((state) => state.productCategories);

  useEffect(() => {
    if (show) {
      setReqState({
        hidden: !show,
        loading: false,
        success: false,
      });
    } else {
      setTimeout(() => {
        setSelectedCategory(undefined);
        setBlobImage(undefined);
        setSuccess(false);
        setImage("");
        setReqState({
          hidden: true,
          loading: false,
          success: false,
        });
      }, 700);
    }
  }, [show]); 

  useEffect(() => {
    dispatch(ProductService.getCategories({}));
  }, []);
  return (
    <Dialog maxW="max-w-md" show={show} title="Add Product" onClose={onClose}>
      {!reqState.hidden && (
        <Form
          onSubmit={async (e) => {
            setReqState({
              hidden: true,
              success: false,
              loading: true,
            });
            if (blobImage && selectedCategory) {
              const form = new FormData();
              form.append("name", e.name);
              form.append("description", e.description);
              form.append("productCategoryId", `${selectedCategory.id}`);
              form.append("price", e.price);
              form.append(
                "image",
                blobImage ?? new Blob([], { type: "image/png" })
              );

              const response = await ApiService<Response<undefined>>({
                endpoint: "/products",
                method: "POST",
                data: form,
                useAuth: true,
              });
              if (response.status === 201 || response.status === 200) {
                console.log("DATA", response);
                setReqState({
                  hidden: true,
                  success: true,
                  loading: false,
                });
              } else {
                setReqState({
                  hidden: true,
                  success: false,
                  loading: false,
                });
              }
            }
          }}
        >
          <div
            onClick={() => inputFile.current?.click()}
            className="h-[200px] cursor-pointer active:scale-75 transition-all ease-in-out duration-500 bg-slate-100 flex flex-col items-center justify-center w-full rounded-md overflow-hidden relative"
          >
            {image.length > 5 ? (
              <Image fill={true} objectFit="contain" src={image} alt="upload" />
            ) : (
              <MdAddAPhoto className="text-[64px] text-violet-500" />
            )}
          </div>
          <input
            id="upload"
            type="file"
            ref={inputFile}
            accept="image/png, image/jpg, image/jpeg"
            onChange={(event) => {
              if (event.target.files) {
                let fg = event.target.files[0];
                if (fg && fg.type) {
                  fg.arrayBuffer().then((arrayBuffer) => {
                    const blob = new Blob([new Uint8Array(arrayBuffer)], {
                      type: fg.type,
                    });
                    setBlobImage(blob);
                    setImage(URL.createObjectURL(blob));
                  });
                }
              }
            }}
            style={{
              display: "none",
            }}
          />
          <Input
            label="Name"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdEditDocument />
              </div>
            }
            config={{
              name: "name",
            }}
          />
          <Input
            label="Description"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdEditDocument />
              </div>
            }
            config={{
              name: "description",
              type: "text",
            }}
          />
          <Select
            idKey=""
            initId={undefined}
            label="Category"
            data={data ?? []}
            keyView="name"
            onSelected={(data: ProductCategory) => {
              setSelectedCategory(data);
            }}
          />
          <Input
            label="Price"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdPriceChange />
              </div>
            }
            config={{
              name: "price",
              type: "number",
            }}
          />
          <div className="p-4 w-full flex flex-col items-center">
            <button className="px-4 max-w-sm w-full py-3 active:opacity-90 rounded-lg bg-violet-600 text-white">
              Submit
            </button>
          </div>
        </Form>
      )}
      {reqState.loading && (
        <div className="w-full h-[300px] flex flex-col items-center justify-center">
          <Loading show={true} size={64} />
        </div>
      )}
      {reqState.success && (
        <div className="w-full h-[300px] text-slate-700 flex flex-col items-center justify-center">
          <p>Success Add Product</p>
        </div>
      )}
    </Dialog>
  );
};

export default AddProductDialog;
