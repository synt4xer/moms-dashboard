import { useAppSelector } from "@/redux/store";
import Image from "next/image";
import { usePathname, useRouter } from "next/navigation";
import { ReactNode, useEffect, useState } from "react";
import {
  MdArrowBack,
  MdArrowForward,
  MdChevronLeft,
  MdChevronRight,
} from "react-icons/md";

const ContentLayout = ({
  pageTitle,
  children,
}: {
  pageTitle: string;
  children: ReactNode;
}) => {
  const router = useRouter();
  const pathname = usePathname();
  const [path, setPath] = useState<
    {
      name: string;
      path: string;
    }[]
  >([]);

  useEffect(() => {
    let pathTemp = "/";
    const paths = pathname
      .substring(1)
      .split("/")
      .map((p) => {
        pathTemp = pathTemp + p + "/";
        return {
          name: p,
          path: pathTemp.substring(0, pathTemp.length - 1),
        };
      });

    setPath(paths);
  }, [pathname]);

  return (
    <>
      <head>
        <title>{`Mooms - ${pageTitle}`}</title>
      </head>
      <header className="w-full top-0 flex flex-row items-center justify-between left-0 sticky z-[999] bg-white p-4 h-[68px]">
        <div className="flex flex-row items-center justify-start space-x-4">
          <button
            onClick={() => router.back()}
            className="p-2 z-2  bg-violet-600 text-white rounded-full"
          >
            <MdArrowBack size={24} />
          </button>
          <div className="flex flex-col items-start">
            <h3 className="text-xl font-semibold">{pageTitle}</h3>
            <div className="flex flex-row items-center">
              {path.map((e, i) => (
                <div key={e.path + i} className="flex flex-row items-center">
                  <p
                    className="cursor-pointer font-semibold text-md -mt-[2px] text-violet-600"
                    onClick={() => {
                      console.log(e);
                      router.push(e.path);
                    }}
                  >
                    {e.name}
                  </p>
                  {i !== path.length - 1 && (
                    <p className="-mt-[2px] font-semibold text-violet-600 px-1">
                      /
                    </p>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="flex flex-row items-center space-x-4">
          <div className="flex flex-col items-end">
            <h2 className="text-lg hidden sm:inline-block font-medium text-violet-600">
              Kim Jong Un
            </h2>
            <p className="text-sm hidden sm:inline-block -mt-[2px] text-slate-500">
              Administrator
            </p>
          </div>
          <div className="h-[46px] w-[46px] rounded-full relative overflow-hidden">
            <Image
              alt="profile"
              objectFit="cover"
              src={
                "https://asset-2.tstatic.net/aceh/foto/bank/images/0123Pemimpin-Korea-Utara-Kim-Jong-Un.jpg"
              }
              fill={true}
            />
          </div>
        </div>
      </header>
      {children}
      <div className="flex-1 min-h-[64px]" />
      <footer className="pl-[64px] pr-6 hidden w-full border-t-[0.7px] border-t-violet-100 flex-row justify-between items-center py-3 sticky left-0 bg-white bottom-0">
        <div className=" flex flex-col items-start">
          <p>{20}</p>
        </div>
        <div className="flex flex-row items-center space-x-2">
          <div className="px-3 py-2 rounded-xl text-[28px] bg-violet-600 text-white">
            <MdChevronLeft />
          </div>
          {[1, 2, 3, 4].map((e, i) => (
            <div
              key={e}
              className={`px-4 py-2 rounded-xl border-[1px] border-violet-600 ${
                i === 2 ? "bg-violet-600 text-white" : "bg-white"
              } `}
            >
              {e}
            </div>
          ))}
          <div className="px-3 py-2 rounded-xl text-[28px] bg-violet-600 text-white">
            <MdChevronRight />
          </div>
        </div>
      </footer>
    </>
  );
};

export default ContentLayout;
