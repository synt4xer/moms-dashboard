import Loading from "@/components/loading";
import { AuthService } from "@/redux/reducers/auth.reducers";
import { setRefreshToken, setToken } from "@/redux/reducers/token.reducer";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { setCookie } from "@/utils/cookie";
import { useEffect } from "react";
import { MdArrowForward } from "react-icons/md";

const LogoutButton = () => {
  const dispatch = useAppDispatch();
  const { loading, success } = useAppSelector((state) => state.authLogout);
  const { token, refreshToken } = useAppSelector((state) => state.token);
  useEffect(() => {
    if (success) {
      dispatch(setToken(""));
      dispatch(setRefreshToken(""));
      setCookie("SessionID", "");
      window.location.reload();
    }
  }, [success, dispatch]);
  return (
    <div className="w-full flex justify-between px-4 pb-3 flex-row items-center">
      <p className="text-lg font-bold text-violet-600">Sign Out</p>
      <div
        onClick={() =>
          dispatch(
            AuthService.logout({
              data: {
                token: token,
                refreshToken: refreshToken,
              },
            })
          )
        }
        className="relative flex flex-col w-[56px] h-[68px] items-center justify-center"
      >
        <Loading show={loading ?? false} size={56} />
        <button
          disabled={loading ?? false}
          className="p-2 absolute z-2  bg-violet-600 text-white rounded-full"
        >
          <MdArrowForward size={24} />
        </button>
      </div>
    </div>
  );
};

export default LogoutButton;
