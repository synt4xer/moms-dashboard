import Image from "next/image";
import moomsLogo from "@/assets/logo.svg";

const Logo = () => {
  return (
    <div className="p-6 w-full">
      <div className="w-full aspect-square relative">
        <Image src={moomsLogo} fill={true} alt="logo" objectFit="contain" />
      </div>
    </div>
  );
};

export default Logo;
