"use client";

import { MdDelete, MdEdit, MdVisibility } from "react-icons/md";
import ContentLayout from "../content";

const page = () => {
  return (
    <ContentLayout pageTitle="Inventory">
      <div className="w-full h-[160px] sticky left-0 bg-amber-100"></div>
      <table className="table-responsive w-full">
        <thead className="bg-violet-600 font-bold text-center sticky top-[68px] left-0 text-white">
          <tr>
            <td className="px-3 py-3">Name</td>
            <td className="px-3 py-3">Description</td>
            <td className="px-3 py-3">Description</td>
            <td className="px-3 py-3">Description</td>
            <td className="px-3 py-3">Description</td>
            <td className="px-3 py-3">Price</td>
            <td className="px-3 py-3">Voucher</td>
            <td className="px-3 py-3">Actions</td>
          </tr>
        </thead>
        <tbody>
          {[...Array(100)].map((e, i) => (
            <tr
              key={e + i}
              className={`${
                i % 2 === 0 ? "bg-white" : "bg-slate-100/70"
              } hover:bg-violet-200`}
            >
              <td className="px-3 py-3">Lorem Ipsum</td>
              <td>Lorem ipsum dolor sit amet</td>
              <td>Lorem ipsum dolor sit amet</td>
              <td>Lorem ipsum dolor sit amet</td>
              <td className="text-center">Rp 30.000</td>
              <td className="text-center">THBRU</td>
              <td className="flex py-3 px-3 flex-row text-slate-700 text-[24px] items-center justify-center space-x-4">
                <MdVisibility />
                <MdEdit />
                <MdDelete className="text-red-500" />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </ContentLayout>
  );
};

export default page;
