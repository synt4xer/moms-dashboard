"use client";
import Loading from "@/components/loading";
import { usePathname, useRouter } from "next/navigation";
import { ReactNode, useEffect, useState } from "react";

import {
  MdAlbum,
  MdArrowForward,
  MdChevronLeft,
  MdDashboard,
  MdDiscount,
  MdGroups,
  MdLocalShipping,
} from "react-icons/md";
import Logo from "./component/logo";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { AuthService } from "@/redux/reducers/auth.reducers";
import { setRefreshToken, setToken } from "@/redux/reducers/token.reducer";
import { setCookie } from "@/utils/cookie";
import LogoutButton from "./component/logout.button";

interface Menu {
  id: number;
  title: string;
  path: string;
  icon: ReactNode;
  selected: boolean;
}

const initMenu: Menu[] = [
  {
    id: 1,
    title: "Dashboard",
    path: "/dashboard",
    icon: <MdDashboard />,
    selected: true,
  },
  {
    id: 2,
    title: "Products",
    path: "/dashboard/products",
    icon: <MdAlbum />,
    selected: false,
  },
  {
    id: 3,
    title: "Shipments",
    path: "/dashboard/shipments",
    icon: <MdLocalShipping />,
    selected: false,
  },
  {
    id: 4,
    title: "Vouchers",
    path: "/dashboard/vouchers",
    icon: <MdDiscount />,
    selected: false,
  },
  {
    id: 5,
    title: "Users",
    path: "/dashboard/users",
    icon: <MdGroups />,
    selected: false,
  },
];

const layout = ({ children }: { children: ReactNode }) => {
  const pathname = usePathname();
  const router = useRouter();
  const [menu, setMenu] = useState(initMenu);
  const [collapse, setCollapse] = useState(false);
  const [isOverlay, setIsOverlay] = useState(false);

  const setDataMenu = (path: string) => {
    const newMenu = initMenu.map((e) => {
      return {
        ...e,
        selected: e.path === path,
      };
    });
    setMenu(newMenu);
  };

  useEffect(() => {
    if (pathname) {
      setDataMenu(pathname);
    }
  }, [pathname]);

  useEffect(() => {
    window.addEventListener("resize", (ev) => {
      if (document.body.offsetWidth > 760) {
        setIsOverlay(false);
      } else {
        setIsOverlay(true);
      }
    });
  }, []);

  return (
    <>
      <aside
        className={`bg-slate-50 w-[200px] z-[9999] border-r-[1px] border-r-slate-300/80 fixed top-0 bottom-0 left-0 flex flex-col items-start justify-start
      ${collapse ? "-translate-x-full" : "translate-x-0"}
      transition-all ease-in-out duration-700`}
      >
        <Logo />
        <div className="flex-1 w-full flex-col items-start justify-start">
          {menu.map((e) => (
            <button
              key={e.path}
              onClick={() => router.push(e.path)}
              className={`flex w-full pl-6 py-2 active:scale-110 active:bg-violet-200 items-center space-x-4 ${
                e.selected
                  ? "border-r-violet-600  text-violet-600 font-bold"
                  : "text-slate-600 border-r-slate-50 font-normal hover:bg-violet-100 "
              } transition-all ease-in-out duration-500`}
            >
              <div className="text-[24px]">{e.icon}</div>
              <p className="text-[18px] text-start flex-1">{e.title}</p>
              <div
                className={`h-[36px] w-[2px] ${
                  e.selected ? "bg-violet-600" : "bg-transparent"
                } rounded-full`}
              />
            </button>
          ))}
        </div>
        <LogoutButton />
        <div
          onClick={() => setCollapse(!collapse)}
          className="bg-violet-600 active:opacity-50 cursor-pointer text-white p-2 rounded-tr-lg absolute z-[99] bottom-[10px] -right-[48px] rounded-br-lg text-[32px]"
        >
          <MdChevronLeft
            className={`${
              collapse ? "rotate-180" : "rotate-0"
            } transition-all ease-in-out duration-700`}
          />
        </div>
        <div className=" flex flex-col items-start w-full p-4 border-t-[1px] border-t-violet-100">
          <h1 className="text-md font-bold text-violet-600">Mooms Dashboard</h1>
          <p className="text-sm">v1.0.0</p>
        </div>
      </aside>
      <main
        className={`bg-white absolute top-0 bottom-0 right-0 ${
          collapse || isOverlay ? "left-[0px] " : "left-[200px]"
        } transition-all ease-in-out duration-700`}
      >
        <div className="w-full h-full bg-violet-50 flex flex-col items-start justify-start relative overflow-auto scroll-smooth">
          {children}
        </div>
      </main>
    </>
  );
};

export default layout;
