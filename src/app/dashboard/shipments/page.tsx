"use client";

import { MdDelete, MdEdit, MdVisibility } from "react-icons/md";
import ContentLayout from "../content";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { useEffect } from "react";
import { ShipmentService } from "@/redux/reducers/shipment.reducer";
import { formatToIDR } from "@/utils/converter";

const page = () => {
  const dispatch = useAppDispatch();
  const { data, loading } = useAppSelector((state) => state.shipmemtGetAll);
  useEffect(() => {
    dispatch(ShipmentService.getAll({}));
  }, [dispatch]);
  return (
    <ContentLayout pageTitle="Shipments">
      <div className="w-full h-[160px] sticky left-0 bg-amber-100"></div>
      <table className="table-responsive w-full flex-1">
        <thead className="bg-violet-600 font-bold text-center sticky top-[68px] left-0 text-white">
          <tr>
            <td className="px-3 py-3">Name</td>
            <td className="px-3 py-3">Code</td>
            <td className="px-3 py-3">Amount</td>
            <td className="px-3 py-3">Status</td>
            <td className="px-3 py-3">Actions</td>
          </tr>
        </thead>
        <tbody className="rounded-b-3xl">
          {data &&
            data.map((e, i) => (
              <tr
                key={e.id}
                className={`${
                  i % 2 === 0 ? "bg-white" : "bg-slate-100/70"
                } hover:bg-violet-200`}
              >
                <td className="px-4 py-3">{e.name}</td>
                <td className="px-3 py-3 text-center font-bold">{e.code}</td>
                <td className="px-3 py-3 text-center">
                  {formatToIDR(parseInt(e.amount ?? "0"))}
                </td>
                <td className="px-3 py-3 text-center">
                  <div className="flex flex-row relative items-center justify-center">
                    <div
                      className={`${
                        e.isActive
                          ? "text-green-600 border-green-500"
                          : "text-red-500 border-red-500"
                      } border-[1px] px-3 py-[2px] font-bold rounded-md w-auto`}
                    >
                      {e.isActive ? "ACTIVE" : "INCATIVE"}
                    </div>
                  </div>
                </td>
                <td className="py-3 px-3 text-center">
                  <div className="flex flex-row text-slate-700 text-[24px] items-center justify-center space-x-4">
                    <MdVisibility />
                    <MdEdit />
                    <MdDelete className="text-red-500" />
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
        <tfoot className="w-full h-[20px]"></tfoot>
      </table>
    </ContentLayout>
  );
};

export default page;
