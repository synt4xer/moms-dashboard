"use client";

import {
  MdDateRange,
  MdDelete,
  MdEdit,
  MdEmail,
  MdLock,
  MdPerson,
  MdPersonAdd,
  MdSearch,
  MdVisibility,
} from "react-icons/md";
import ContentLayout from "../content";
import { useAppDispatch, useAppSelector } from "@/redux/store";
import { useEffect, useState } from "react";
import Input from "@/components/input";
import Dialog from "@/components/dialog";
import Form from "@/components/form";
import { UserService } from "@/redux/reducers/user.reducer";
import UserDialog from "./components/user.dialog";
import DataNotFound from "@/components/data.notfound";
import { getDateWWDDMMYYY } from "@/utils/converter";

const page = () => {
  const dispatch = useAppDispatch();
  const { data, loading } = useAppSelector((state) => state.userGetAll);
  const [showAddUser, setShowAddUser] = useState(false);

  useEffect(() => {
    dispatch(UserService.getAll({}));
  }, [dispatch]);
  return (
    <>
      <ContentLayout pageTitle="Users">
        <div className="w-full flex flex-row items-end justify-between sticky p-4 left-0 bg-white">
          <div>
            <Input
              maxWidth="max-w-xs"
              label="Search"
              config={{
                name: "search",
                autoComplete: "false",
                autoFocus: false,
                type: "text",
              }}
              trailing={
                <div className="flex cursor-pointer active:opacity-60 flex-row items-center justify-center w-[56px] h-full text-center text-[32px] bg-violet-600 text-white">
                  <MdSearch />
                </div>
              }
            />
          </div>
          <button
            onClick={() => setShowAddUser(true)}
            className="flex bg-violet-600 rounded-lg text-white px-4 h-[46px] flex-row items-center space-x-3"
          >
            <p>Add User</p>
            <MdPersonAdd size={24} />
          </button>
        </div>
        {data && data.length > 0 ? (
          <table className="table-responsive w-full">
            <thead className="bg-violet-600 font-bold text-center sticky top-[68px] left-0 text-white">
              <tr>
                <td className="px-3 py-3 min-w-[200px] whitespace-nowrap">
                  Name
                </td>
                <td className="px-3 py-3 min-w-[200px] whitespace-nowrap">
                  Email
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[160px]">
                  Date Of Birth
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                  Phone
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                  Address
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                  Role
                </td>
                <td className="px-3 py-3 whitespace-nowrap min-w-[120px]">
                  Status
                </td>
                <td className="px-3 py-3 whitespace-nowrap">Actions</td>
              </tr>
            </thead>
            <tbody>
              {data &&
                data.map((e, i) => (
                  <tr
                    key={e.id}
                    className={`${
                      i % 2 === 0 ? "bg-white" : "bg-slate-100/70"
                    } hover:bg-violet-200`}
                  >
                    <td className="px-3 py-3">{e.name}</td>
                    <td className="px-3 py-3">{e.email}</td>
                    <td className="px-3 py-3 text-end">
                      {getDateWWDDMMYYY(new Date(e.dob ?? ""), "id-ID")}
                    </td>
                    <td className="pl-3 pr-6 py-3 text-end">{e.phone}</td>
                    <td className="pl-3 pr-6 py-3 text-end">{e.address}</td>
                    <td className="pl-3 pr-6 py-3 text-center">
                      {e.role?.toUpperCase()}
                    </td>
                    <td className="px-3 py-3 text-center">
                      {e.isActive ? "ACTIVE" : "INCATIVE"}
                    </td>
                    <td className="flex py-3 px-4 flex-row text-slate-700 text-[24px] items-center justify-center space-x-6">
                      <MdVisibility className="text-teal-500 cursor-pointer" />
                      <MdEdit className="text-amber-500 cursor-pointer" />
                      <MdDelete className="text-red-500 cursor-pointer" />
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        ) : (
          <DataNotFound loading={loading} />
        )}
      </ContentLayout>
      <UserDialog show={showAddUser} onClose={() => setShowAddUser(false)} isViewOnly={true} />
    </>
  );
};

export default page;
