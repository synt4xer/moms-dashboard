import Dialog from "@/components/dialog";
import Form from "@/components/form";
import Input from "@/components/input";
import { MdDateRange, MdEmail, MdLock, MdPerson } from "react-icons/md";
import SelectRole from "./role.select";
import { User } from "@/model/auth";

const UserDialog = ({
  show,
  data,
  isViewOnly,
  onClose,
}: {
  onClose?: () => void;
  data?: User;
  isViewOnly: boolean;
  show: boolean;
}) => {
  return (
    <Dialog show={show} title="Add User" onClose={onClose}>
      <Form onSubmit={(e) => {}}>
        <Input
          label="Name"
          leading={
            <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
              <MdPerson />
            </div>
          }
          config={{
            name: "name",
            value: isViewOnly ? data?.name : undefined,
          }}
        />
        <Input
          label="Email"
          leading={
            <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
              <MdEmail />
            </div>
          }
          config={{
            name: "email",
            value: isViewOnly ? data?.email : undefined,
          }}
        />
        {!isViewOnly && (
          <Input
            label="Password"
            leading={
              <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
                <MdLock />
              </div>
            }
            config={{
              name: "password",
              type: "password",
            }}
          />
        )}
        <Input
          label="Date Of Birth"
          leading={
            <div className="w-[56px] h-full flex flex-col items-center text-[24px] justify-center bg-violet-600 text-white">
              <MdDateRange />
            </div>
          }
          config={{
            name: "dob",
            type: "date",
            value: isViewOnly ? data?.dob?.toDateString() : undefined,
            lang: "id-ID",
          }}
        />
        <SelectRole
          selectedDefault={isViewOnly ? data?.role : undefined}
          keyView="role"
          data={[
            {
              id: 1,
              role: "Super Admin",
            },
            {
              id: 2,
              role: "Administrator",
            },
            {
              id: 3,
              role: "Member",
            },
          ]}
          onSelected={(e) => {}}
        />
        <div className="p-4 w-full flex flex-col items-center">
          <button className="px-4 max-w-sm w-full py-3 rounded-lg bg-violet-600 text-white">
            Submit
          </button>
        </div>
      </Form>
    </Dialog>
  );
};

export default UserDialog;
