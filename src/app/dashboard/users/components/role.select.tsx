import Input from "@/components/input";
import { useEffect, useRef, useState } from "react";
import {
  MdArrowDropDown,
  MdArrowDropDownCircle,
  MdDateRange,
  MdRocket,
} from "react-icons/md";

const SelectRole = ({
  data,
  keyView,
  selectedDefault,
  onSelected,
}: {
  data: any[];
  selectedDefault?: string;
  keyView: string;
  onSelected: (data: any) => void;
}) => {
  const reff = useRef<HTMLDivElement>(null);
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState<any>();

  useEffect(() => {
    const clickHandler = (event: MouseEvent) => {
      if (
        reff.current &&
        !reff.current.contains(event.target as HTMLDivElement)
      ) {
        setShow(false);
      }
    };
    document.addEventListener("mousedown", clickHandler);
    return () => {
      document.removeEventListener("mousedown", clickHandler);
    };
  });

  useEffect(() => {
    if (selectedDefault) {
      
    }
  }, [selectedDefault]);

  return (
    <div className="w-full relative">
      <Input
        label="Role"
        viewOnly={true}
        leading={
          <div className="w-[56px] h-full flex flex-col items-center text-[28px] justify-center bg-violet-600 text-white">
            <MdRocket />
          </div>
        }
        config={{
          name: "role",
          type: "text",
          value: selected ? selected[keyView] : "",
          lang: "id-ID",
        }}
        trailing={
          <div
            onClick={() => setShow(!show)}
            className="text-[24px] cursor-pointer text-violet-500 px-3 h-full flex flex-col items-center justify-center"
          >
            <MdArrowDropDownCircle />
          </div>
        }
      />
      <div
        ref={reff}
        className={`bg-white w-full absolute border-slate-100 border-[0.7px] overflow-hidden bottom-[52px] flex flex-col items-center divide-y-[1px] divide-violet-100 rounded-lg shadow-xl transition-all ease-in-out duration-700`}
      >
        {show && (
          <div className="px-4 py-3 w-full text-center text-violet-600 font-bold">
            Select Role
          </div>
        )}
        {show &&
          data.map((e) => (
            <div
              key={e[keyView]}
              onClick={() => {
                setSelected(e);
                setShow(false);
              }}
              className="px-4 py-3 w-full hover:bg-violet-600 hover:text-white"
            >
              {e[keyView]}
            </div>
          ))}
      </div>
    </div>
  );
};

export default SelectRole;
