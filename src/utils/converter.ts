// Senin, 12 Januari 2024
export const getDateWWDDMMYYY = (date: Date, locale: string) => {
  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  } as any;
  return date.toLocaleDateString(locale, options);
};

// 12 Januari 2025
export const getDateDDMMYYY = (date: Date, locale: string) => {
  const options = {
    year: "numeric",
    month: "long",
    day: "numeric",
  } as any;
  return date.toLocaleDateString(locale, options);
};

///////
export const fileToBase64 = (
  file: File,
  onSelected: (result: string | ArrayBuffer | null) => void,
  onError: (error: string) => void
) => {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = function () {
    onSelected(reader.result);
  };
  reader.onerror = function () {
    onError("Image invalid!");
  };
};

export const imageSizeReducer = async (
  base64Str: string,
  MAX_WIDTH = 520,
  MAX_HEIGHT = 520
) => {
  let resized_base64 = await new Promise((resolve) => {
    let img = new Image();
    img.src = base64Str;
    img.onload = () => {
      let canvas = document.createElement("canvas");
      let width = img.width;
      let height = img.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }
      canvas.width = width;
      canvas.height = height;
      let ctx = canvas.getContext("2d");
      if (ctx) {
        ctx.drawImage(img, 0, 0, width, height);
        resolve(canvas.toDataURL());
      }
      return canvas.toDataURL("image/png");
    };
  });
  return `${resized_base64}`;
};

const formatter = new Intl.NumberFormat("id-ID", {
  style: "currency",
  currency: "IDR",
  maximumFractionDigits: 0,
});

export const formatToIDR = (value: number | bigint) => {
  return formatter.format(value);
};
